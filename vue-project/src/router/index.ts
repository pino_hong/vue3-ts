import { createRouter, createWebHistory,useRouter } from 'vue-router'
import type {RouteRecordRaw} from 'vue-router'
import Home from '../views/home.vue'
import type { TabbarPathItem } from '@/tsUtils/index';
const tabbar:Array<TabbarPathItem> = [{
  path: '/home',
  text: '首页',
  iconPath: 'https://codegeex.cn',
  selectedIconPath: 'https://codegeex.cn' 
},{
  path:'/shopcart',
  text:'购物车',
  iconPath: 'https://codegeex.cn',
  selectedIconPath: 'https://codegeex.cn'
},{
  path:'/my',
  text: '我的',
  iconPath: 'https://codegeex.cn',
  selectedIconPath: 'https://codegeex.cn'
}]
const routes:RouteRecordRaw[] = [
  {
    path: '/',
    component: Home,
    meta:{
      istabbar:true,
      title:'首页'
    }
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/home.vue'),
    meta:{
      istabbar:true,
      title:'首页'
    }
  },
  {
    path: '/shopcart',
    name: 'shopcart',
    component: () => import('../views/shopcart.vue'),
    meta:{
      istabbar:true,
      title:'购物车'
    }
  },
  {
    path: '/my',
    name: 'my',
    component: () => import('../views/my.vue'),
    meta:{
      istabbar:true,
      title:'个人中心'
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login.vue'),
    meta:{
      istabbar:false,
      title:'登录'
    }
  },
  {
    path: '/list',
    name: 'list',
    component: () => import('../views/list.vue'),
    meta:{
      istabbar:false
    }
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: () => import('../views/detail.vue'),
    meta:{
      istabbar:false,
      title:'商品详情'
    }
  },
]
// const useRouter = useRouter()
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes:routes
})
try{
  for (let i = 0; i < tabbar.length; i++) {
    const item = tabbar[i];
    if(!routes.some(some_item=>some_item.path===item.path)){
      throw new Error(`tabbar中的${item.path}没有在routes表中注册，不存在这个路由`)
      break;
    }
  }
}catch(err){
  console.error(err)
}
// router.beforeEach((to,form,next)=>{
//   console.log(to,'-------')
//   next()
// })

export {
  router,
  tabbar,
}
