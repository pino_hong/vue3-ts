export interface Coord {
  x: number;
  y: number;
}

export interface Paramas {
  [key:string]:any
}

export interface PageParams {
  _page: number,
  _limit: number,
  keyword?:string
}