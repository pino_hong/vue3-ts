export interface TabbarPathItem {
  path: string;
  text: string;
  iconPath: string;
  selectedIconPath: string;
}
