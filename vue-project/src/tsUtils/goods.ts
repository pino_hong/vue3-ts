
export interface GoodsParams {
  id: string,
  image: string,
  store_name: string,
  now_price_fmt: string,
  title: string,
  // like?:boolean,
  [key: string]: any
}

export interface CartGoodsParams extends GoodsParams {
  count?: number | undefined,
  check?: boolean | undefined
}
