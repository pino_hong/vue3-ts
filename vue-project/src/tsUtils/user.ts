export interface Account {
  username:string,
  pwd: string,
}
export interface User extends Account{
  id: number | string,
  nickname: string,
  avatar: string,
  token: string
}

