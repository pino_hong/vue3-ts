
// import {useRouter} from 'vue-router'
// import { store } from '@/store'
import axios, { type InternalAxiosRequestConfig,type AxiosRequestConfig } from 'axios'
import { ElMessage, ElMessageBox } from 'element-plus'
// const router = 	useRouter()


const request = axios.create({
  baseURL: 'http://localhost:3000'||import.meta.env.VITE_API_BASE_URL
})
// request 不支持泛型
// request.get post put 支持响应数据泛型
// 由于我们的后端又包装了一层数据data ，导致访问数据比较麻烦，所以又封装一层
// 控制登录过期的锁
let isRefreshing = false
// 请求拦截器
request.interceptors.request.use((config:InternalAxiosRequestConfig) => {
  // 请求头添加token
  const token = localStorage.getItem('token')
  if (token) {
    // config.headers['Authorization'] = `Bearer ${token}`
    // config.headers.set('Authorization',`Bearer ${token}`)
  }
  config.headers.set('x-channel',`wepublishers_client-wxd2b68a10b02b2ab6/1.0.24`)
  return config
}, error => {
  return Promise.reject(error)
})

// 响应拦截器
request.interceptors.response.use(response => {
  const status = response.data.status
  if (!status || status === 200) {
    return response
  }
  if (status === 401) {
    // 防止多次弹出登录框
    if (isRefreshing) return Promise.reject(response)
    // 清除本地过去的登录状态
    ElMessageBox.confirm('登录状态已过期，请重新登录', '提示', {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type: 'warning'
    }).then(() => {
      // console.log('已跳转')
      // 跳转到登录页
      // store.commit('setUser', null)
      // router.push({
      //   name: 'login',
      //   query: {
      //     // 方便登录成功后回到当前页面
      //     redirect: router.currentRoute.value.fullPath
      //   }
      // })
    }).catch(() => {
      // 抛出异常
    })
    // isRefreshing = false
    // 在内部消化掉这个业务异常
    return Promise.reject(response)
  }

  // 其他错误情况
  ElMessage.error(response.data.msg || '请求失败，请稍后重试')
  return Promise.reject(response.data)
}, error => {
  ElMessage.error(error.message)
  return Promise.reject(error)
})

// 通过导出自定的request实例，来实现对axios的类型封装
export default <T = any>(config:AxiosRequestConfig) => {
  return request(config).then(res => {
    return (res.data.data || res.data) as T
  })
}