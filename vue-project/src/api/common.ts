import http from './http/index'
import type { PageParams,GoodsParams,Account,User } from '@/tsUtils'
import {stringifyUrl} from '@/assets/js/common'
import { showToast } from 'vant'

export const api_get_Settings = () => {
  return http({
    url: "/publisher/settings",
    method: 'get'
  }) 
}

export const api_get_goodslist = (data:PageParams) => {
  return http<GoodsParams[]>({
    url:stringifyUrl('/goods',data)
  })
}

export const api_login = (account:Account) => {
  return http<User[]>({
    url:`/user?username=${account.username}&pwd=${account.pwd}`
  })
}

export const api_register = async (data:User) => {
  console.log('注册了')
  const login_result = await http<User[]>({
    url:`/user?username=${data.username}`
  })
  if(login_result.length) {
    showToast('账号已存在')
    return 
  }
  const result = await http<User>({
    url:`/user`,
    method:'post',
    data
  })
  return result
}

export const api_get_goodsdetail = (id:string|number) => {
  return http<GoodsParams[]>({
    url:`/goods?id=${id}`
  })
}

export const api_set_goodslike = (id:string,data:GoodsParams) => {
  return http<GoodsParams>({
    url:`/goods/${id}`,
    data,
    method:"patch"
  })
}
