import type { Paramas } from '@/tsUtils/index'
export function setRem() {
  // 设计稿宽度
  const WIDTH = 750 
  // 当前页面宽度相对于 750 宽的缩放比例，可根据自己需要修改
  const scale:number = document.documentElement.clientWidth / WIDTH;
  document.documentElement.style.fontSize = scale + 'px';
}
// 解析url
export function parseUrl(str:string){
  if(str.indexOf('?')>-1){
    let str_arr = str.split('?')
    let params = str_arr[1].indexOf('&')>-1?str_arr[1].split('&'):[str_arr[1]]
    let params_arr = params.map(item => item.split('='))
    const obj:Paramas = {}
    params_arr.forEach(item=>{
      if(item[1]){
        obj[item[0]] = item[1]
      }
    })
    return {
      url:str_arr[0],
      query:obj
    }
  }
  return str
}
// 拼接url
export function stringifyUrl(url:string,params:object){
  let str = ''
  if (Object.keys(params).length) {
    Object.entries(params).forEach((item:Array<string|number>, index:number) => {
      if (Object.keys(params).length - 1 === index) {
        str += `${item[0]}=${item[1]}`
      } else {
        str += `${item[0]}=${item[1]}&`
      }
    })
  }
  return str?`${url}?${str}`:url
}

export function deepcopy<T>(obj:Paramas):T{
  console.log(obj)
  const map = new WeakMap()
  const fn = (params:Paramas) => {
    if (typeof params !== 'object') return params
    const obj:any = Array.isArray(params) ? [] : {};
    const val = null
    // 阻止循环引用导致的爆栈问题。
    if (map.has(params)) {
      return map.get(params)
    }
    map.set(params, val)

    for (const key in params) {
      obj[key] = typeof params[key] === "object" ?
        fn(params[key]) :
        params[key];
    }
    return obj;
  }
  return fn(obj)
}