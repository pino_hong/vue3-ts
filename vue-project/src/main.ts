import { createApp } from 'vue'
import { createPinia } from 'pinia'
// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import {setRem} from '@/assets/js/common.js'
import 'vant/lib/index.css'
import vant from 'vant'
import App from './App.vue'
import {router} from './router'
import piniaPersist from 'pinia-plugin-persist'

// import './assets/main.css'
import './assets/css/iconfont.css'
import './assets/css/reset.css'

import scroll from '@/components/scroll.vue'
setRem();
window.onresize = setRem;
const app = createApp(App)


app.component('my-scroll',scroll)
const pinia = createPinia()
pinia.use(piniaPersist)
app.use(pinia)
app.use(router)
// app.use(ElementPlus)
app.use(vant)

app.mount('#app') 
