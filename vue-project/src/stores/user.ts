import { defineStore } from 'pinia'
import { reactive, ref,computed } from 'vue'
import type { User } from '@/tsUtils'
import { showConfirmDialog } from 'vant'
interface UserState {
  userInfo: User,
  token: string
}
export const useUserStore = defineStore('user', () => {
  const state = reactive<UserState>({
    userInfo: {
      id: "",
      nickname: "",
      username: "",
      avatar: "",
      pwd: "",
      token: ""
    },
    token: ""
  })

  const is_login = computed(() => !!state.token)

  const set_userinfo = (res: User) => {
    state.userInfo = res
    state.token = res.token
  }

  const out_login = async () => {
    showConfirmDialog({
      title: '提示',
      message:'确定退出？',
    })
      .then(() => {
        state.userInfo = {
          id: "",
          nickname: "",
          username: "",
          avatar: "",
          pwd: "",
          token: ""
        }
        state.token = ""
      })
      .catch(() => {
        console.log('取消了')
      });
  }

  return {
    state,
    is_login,
    set_userinfo,
    out_login,
  }
}, {
  persist: {
    enabled: true, // store存储配置，
    strategies: [
      { storage: localStorage, paths: ["state"] },
    ]
  }
})