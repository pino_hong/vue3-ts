import { reactive } from 'vue'
import { defineStore } from 'pinia'
interface RouterState {
  title:string,
  tabbar_current:number
}
export const useRouterStore = defineStore('router',()=>{
  const state = reactive<RouterState>({
    title:"",
    tabbar_current:0
  })

  return {
    state
  }
})