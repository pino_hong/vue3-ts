import {ref,computed, reactive} from 'vue'
import { defineStore } from 'pinia'
import type { CartGoodsParams } from '@/tsUtils'
import {showNotify,showDialog} from 'vant'

interface CardState {
  list:CartGoodsParams[],
  checkAll:boolean,
}
export const useCartStore = defineStore('cart',()=>{
  const state = reactive<CardState>({
    list:[],
    checkAll:false,
  })

  const total_pic = computed(() => {
    const list = state.list
    let pic = 0
    list.forEach((item)=>{
      if(item.check&&item.count){
        pic += (item.count*item.now_price)/100
      }
    })
    return pic
  })

  const count = computed(()=>{
    return state.list.length
  })

  const addCartItem = (item:CartGoodsParams) => {
    const cartItem = state.list.find(v=>v.id === item.id)
    if(cartItem?.count){
      cartItem.count++
      showNotify({ type: 'success',message: `${item.title} x ${cartItem.count}` })
      return
    }
    item.count = 1
    item.check = false
    state.list.unshift(item)
    showNotify({ type: 'success',message: `${item.title} 已添加至购物车` })
  }

  const delCardItem = (index:number) => {
    showDialog({
      title: '提示',
      message: '确定删除该条商品？',
    }).then(() => {
      state.list.splice(index,1)
    });
  }

  const allCheck = (type:boolean) => {
    state.list.forEach((item)=>{
      item.check = type
    })
    state.checkAll = type
  }

  const is_check_all = () => {
    const list = state.list
    state.checkAll = list.every(item => item.check)
  }

  return {
    state,
    total_pic,
    count,
    is_check_all,
    allCheck,
    addCartItem,
    delCardItem
  }
},{
  persist:{
    enabled:true, // store存储配置，
    strategies:[
      { storage: localStorage, paths: ["state"] },
    ]
  }
})