import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useNavTitleStore = defineStore('counter', () => {
  const navTitle = ref('')

  const set_navTitle = (title:string) => {
    navTitle.value = title
  }

  return {
    navTitle,
    set_navTitle
  }
  // const count = ref(0)
  // const doubleCount = computed(() => count.value + 2)
  // function increment() {
  //   count.value++
  // }
  // return { count, doubleCount, increment }
})
